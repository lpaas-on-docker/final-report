%% Direttive TeXworks:
% !TeX root = ../main.tex
% !TeX encoding = UTF-8 Unicode
% !TeX spellcheck = it-IT

% arara: pdflatex: { synctex: yes, shell: yes, interaction: nonstopmode }
% arara: bibtex
% arara: pdflatex: { synctex: yes, shell: yes, interaction: nonstopmode }
% arara: pdflatex: { synctex: yes, shell: yes, interaction: nonstopmode }

\subsection{Ipotesi implicite}\label{subsec:implicit:hypotesis}

Il funzionamento del software prodotto è strettamente legato alla correttezza di funzionamento di LPaaS\@:
in particolare, dalla logica implementativa utilizzata per il dialogo con il livello di persistenza dipende la corretta gestione dello stato in un contesto di distribuzione.

\subsection{Requisiti impliciti}\label{subsec:implicit:requirements}

Si prevede di mantenere lo stato attuale sistema, eventualmente integrando nuove versioni delle librerie utilizzate
(una su tutti, \tuprolog), ma senza variare le funzionalità offerte al di fuori del contesto di distribuzione tramite Docker.

Collegato a ciò, è implicitamente richiesto che le scelte di progettazione e tecnologiche preesistenti, in particolare nel contesto server e persistenza, vengano rispettate:
è implicitamente necessario rimanere, per quanto possibile, all'interno del contesto di Java EE e mantenere la compatibilità con RDBMS SQL\@.

\subsection{Requisiti principali}\label{subsec:requirements}

Durante la fase di analisi dei requisiti sono stati individuati i seguenti requisiti imprescindibili:

\begin{itemize}
  \item devono essere realizzate immagini Docker a partire dalle quali possano essere istanziati container in grado di eseguire su piattaforma Docker.
  \item i container Docker che eseguono LPaaS devono garantire uno stato consistente tra le repliche qualora eseguite in contesto Docker Swarm.
  \item i container che eseguono LPaaS devono essere resilienti agli errori e il servizio, dal punto di vista dei client, deve rimanere disponibile.
  \item il servizio messo a disposizione da un Docker Swarm, dal punto di vista dei client, deve essere trasparente e agire come una singola istanza del server LPaaS.
  \item LPaaS, una volta adattato, deve essere compatibile con l'ultima versione rilasciata di \tuprolog, per quanto possibile.
\end{itemize}

\subsection{Tecnologie}\label{subsec:tech}

\subsubsection{REST}\label{subsub:rest}

LPaaS è un applicativo server che costituisce una \textit{façade} per il motore \tuprolog{}~\cite{tuprolog-padl01} attraverso API REST\@.

Con API REST (o ReST, Representational State Transfer~\cite{rest}) si intende un'astrazione degli elementi architetturali di un sistema ipermediale distribuito su larga scala (per esempio, su internet),
che pone l'accento sul concetto di componenti che interagiscono tramite trasferimento di risorse in uno specifico formato e ad uno specifico percorso (l'URI~\cite{RFC2396}).

Punto interessante dell'architettura REST è il non prevedere il concetto di sessione ed essere, dunque, considerabili \textbf{stateless};
tale caratteristica permette un incapsulamento del codice migliore, permettendo la realizzazione di servizi \textit{loosely-coupled} che possono essere eseguiti in ambienti separati, come ad esempio i container.

\subsubsection{Docker}\label{subsub:docker}

Il progetto open source Docker~\cite{docker} viene rilasciato nel 2013 da una compagnia chiamata dotCloud (ora rinominata Docker), che lavorava su software di tipo \textit{PaaS} (Platform as a service) per il cloud computing.

Docker si pone come obiettivo di automatizzare lo sviluppo di applicazioni
all’interno di \textit{container} software, sfruttando tutti i vantaggi di una virtualizzazione
a livello di sistema operativo; per riuscire a fare ciò, si avvale delle funzionalità di
isolamento presenti nel kernel Linux, come ad esempio \texttt{cgroups}\footnote{\url{https://en.wikipedia.org/wiki/Cgroups}}, utilizzato per la
gestione di risorse fisiche, e \texttt{namespaces}\footnote{\url{https://en.wikipedia.org/wiki/Linux\_namespaces}}, che invece garantiscono un isolamento a
livello di processo.

Grazie a diverse tecniche messe in gioco, come ad esempio l'utilizzo del filesystem \textit{UnionFS}\footnote{\url{https://en.wikipedia.org/wiki/UnionFS}} per permettere di sovrapporre i filesystem di diverse immagini o l'utilizzo della virtualizzazione a livello di sistema operativo, Docker mette a disposizione \textit{containers} leggeri dal punto di vista delle risorse, di facile costruzione e modifica da parte degli utenti e infine altamente compatibili con la maggior parte delle piattaforme standard di cloud computing (AWS, Google Cloud Platform, OpenStack\ldots).
Passando poi all'effettivo funzionamento del sistema Docker, il cuore di tutto può essere identificato nel modulo \textit{Docker Engine}~\cite{docker-engine}, la cui architettura viene descritta in \Cref{fig:dockerArchitectureLabel}.

\begin{figure}[H]
  \includegraphics[scale=0.4]{./res/dockerArchitecture.png}
  \caption{Architettura del sistema Docker}%
  \label{fig:dockerArchitectureLabel}
\end{figure}

Le componenti principali all'interno di \textit{Docker Engine} sono di fatto tre:

\begin{itemize}

  \item
    \textbf{Docker Daemon}:
    demone eseguito in background che si occupa della maggior parte del lavoro, gestendo sotto comando tutti gli oggetti del sistema;

  \item
    \textbf{Docker CLI}:
    componente col compito di fornire un insieme di comandi all’utente per poter istruire il demone sul da farsi;
    comunica con \textit{Docker Daemon} via socket se locale, o via API HTTP se remoto.

  \item
    \textbf{Docker Registry}:
    registro che si occupa della memorizzazione delle \textit{Docker Images}.
    Docker mette a disposizione un registro pubblico, chiamato \textit{DockerHub}, in cui ognuno può postare le proprie immagini e scaricare quelle di altri utenti.
    Viene contattato da \textit{Docker Daemon} nel momento in cui quest'ultimo non disponga di una versione locale dell'immagine richiesta dall'utente.

\end{itemize}

Parlando poi del ciclo di vita dei \textit{container} gestiti dall'architettura sopra descritta, è importante sottolineare l'approccio \textit{stateless} nella gestione delle modifiche allo stato interno di questi ultimi:
Docker infatti non prevede di default un meccanismo di conservazione dello stato tra diverse istanze della stessa immagine, al contrario ogni \textit{container} basato su quest'ultima viene gestito indipendentemente dagli altri.

\subsubsection{Java EE e Payara}\label{subsub:payara}

LPaaS è realizzato secondo le specifiche Java EE~\cite{javaee} (o J2EE, Java Platform Enterprise Edition) e si appoggia all'\textit{application server} Payara.

Payara\footnote{\url{https://www.payara.fish/}} è un progetto derivato da Glassfish, implementazione di riferimento delle API di Java EE realizzata da Oracle stessa, che permette il deploy di applicazioni Java Enterprise compilate in formato \texttt{war};
sono disponibili 2 differenti versioni:

\begin{description}
  \item[Payara Server Full]
    versione completa di Payara Server, che permette configurazioni anche complesse tramite linea di comando o interfaccia web.

  \item[Payara Micro]
    versione di Payara Server orientata ai microservizi e dunque estremamente minimale (pesa meno di \SI{80}{\mega{}\byte{}});
    permette esclusivamente il deploy di file \texttt{war} e supporta configurazioni tramite variabili d'ambiente ed Eclipse MicroProfile\footnote{\url{https://microprofile.io/}},
    non richiedendo dunque alcun tipo di installazione.
\end{description}

La versione preferita per la realizzazione del progetto è Payara Micro:
infatti, le dimensioni contenute e la velocità di avvio la rendono perfetta per l'esecuzione all'interno di container Docker;
inoltre la possibilità di prelevare il \texttt{jar} da Maven per il deploy diretto del \texttt{war} su di esso permette l'utilizzo di configurazioni Gradle per il lancio dell'applicazione in fase di sviluppo,
senza richiedere l'installazione del server sulla macchina locale dello sviluppatore.
