%% Direttive TeXworks:
% !TeX root = ../main.tex
% !TeX encoding = UTF-8 Unicode
% !TeX spellcheck = it-IT

% arara: pdflatex: { synctex: yes, shell: yes, interaction: nonstopmode }
% arara: bibtex
% arara: pdflatex: { synctex: yes, shell: yes, interaction: nonstopmode }
% arara: pdflatex: { synctex: yes, shell: yes, interaction: nonstopmode }

Al termine della fase di analisi dei requisiti, è risultato evidente che il progetto non richiedesse lo sviluppo di complesse strutture software, quanto più l'integrazione del progetto preesistente con uno strumento di deploy, ossia Docker.

Nelle \nameCrefs{subsec:structure} successive, dunque, si analizzerà anche parti della struttura preesistente rilevanti all'integrazione con Docker, oltre alle eventuali modifiche necessarie per l'integrazione con quanto realizzato.


In primo luogo occorre sottolineare che il principale problema emerso in fase di analisi riguardava la gestione dello stato interno del sistema:
nel momento in cui LPaaS veniva distribuito su diverse istanze, mantenere una coerenza dello stato interno tra le varie repliche era sicuramente la sfida più grande.

Successivamente va ricordato che i \textit{container} Docker sono progettati per essere \textit{stateless}, quindi trascurare la gestione dello stato.
Infine vale la pena spendere qualche parola sulla gestione dello stato interno di LPaaS, argomento che verrà poi approfondito nel dettaglio nella sezione riguardante l'implementazione effettiva, per ora basti sapere che l'intero stato di LPaaS è affidato a un database interno messo a disposizione dal server Payara.

L'obiettivo del nostro lavoro era quindi decisamente ambizioso: integrare tutti i vantaggi dell'approccio a \textit{containers} e le relative piattaforme per la distribuzione con una gestione coerenete e efficace dello stato interno del sistema.


\subsection{Struttura}\label{subsec:structure}

La soluzione da noi proposta è stata progettata secondo il paradigma \textit{Separation of Concerns}, modello che prevede la separazione delle funzionalità di un sistema in diversi moduli.

Analizzando a fondo l'implementazione di LPaaS, ci siamo resi conto che non solo il sistema in questione era stato progettato già secondo il pattern sopracitato,
ma che addirittura la separazione tra \textit{Buisness Logic} e stato interno del sistema era particamente già implementata: infatti ogni memorizzazione di dati a lungo termine veniva fatta sfruttando il database interno di Payara,
senza utilizzare strutture dati \textit{in-memory} se non in alcuni passaggi non di vitale importanza.

Alla luce di ciò, LPaaS è stato diviso nei seguenti moduli:

\begin{itemize}

\item \textbf{LPaaS Business Logic module}: Tutte le funzionalità esposte dal sistema LPaaS, le quali delegano al modulo database la persistenza dello stato,
rendendo difatti questo modulo etichettabile come \textit{stateless}, di conseguenza sviluppabile su \textit{container} Docker.

\item \textbf{Database module}: Database a cui il modulo di Buisness Logic affida la gestione dello stato;
essendo per definizione \textit{stateful}, mal si adatta ai \textit{container} Docker.

\end{itemize}

Abbiamo quindi in gioco due moduli, di cui uno è naturalmente predisposto allo sviluppo su \textit{container} mentre l'altro no;
alla luce di ciò abbiamo pensato di strutturare il sistema in due parti separate ma tra di loro comunicanti, come mostrato in figura \Cref{fig:mainarchitectureLabel}
\begin{figure}[H]
  \includegraphics[scale=0.45]{./res/main-architecture-diagram.png}
  \caption{Architettura del sistema LPaaS distribuito}%
  \label{fig:mainarchitectureLabel}
\end{figure}

Dal diagramma in figura emerge una panoramica del sistema nella sua fase finale, in quest'architettura i moduli di Buisness Logic e Database sono stati fisicamente separati in due sottosistemi diversi.
Per quanto riguarda Buisness Logic, esso viene realizzato su un insieme di \textit{containers} gestiti mediante il tool \textit{Docker Swarm mode}, di cui si tratterà approfonditamente nella sezione successiva;
al contrario il modulo di gestione dello stato utilizza tecniche di clustering, replicazione e ridondanza proprie dei database SQL\@.

\subsubsection{Docker Swarm Mode}\label{subsub:swarmmode}

Per ottenere performance ottimali anche nell’ambito distribuito, Docker mette a disposizione una modalità di funzionamento alternativa, chiamata \textit{Swarm mode}.
\textit{Docker swarm mode}~\cite{docker-swarm} si compone di tutto un insieme di tecniche per gestire cluster di \textit{container} Docker distribuiti su rete, sfruttando un insieme di comandi e API, inclusi nel \textit{Docker Engine}, che rendono trasparente al programmatore la parte di configurazione e manutenzione dei vari cluster.
Le differenze di progettazione e funzionamento della \textit{Swarm mode} rispetto alla modalità classica sono molto marcate:
scopo del \textit{container} non è più ospitare l’applicazione nella sua interezza, ma offrire un microservizio che, combinato con altri servizi di nature differenti in esecuzione su \textit{container differenti}, possa fornire le funzionalità dell’applicazione richiesta, seguendo quindi il paradigma di progettazione a microservizi e i conseguenti vantaggi (scalabilità, replicazione, distribuzione\ldots).

Unità base della \textit{Docker Swarm mode} è il nodo, ovvero un’istanza di \textit{Docker Engine} appositamente configurata per funzionare in \textit{Swarm mode} e collegata a un cluster di altri nodi; è fondamentale che quest’istanza possa accedere alla rete e comunicare attraverso di essa.
Ogni nodo poi ha un proprio ruolo:
può essere definito come \texttt{Manager} qualora si voglia assegnargli il compito di coordinare gli altri nodi del cluster, dividendo tra essi i carichi di lavoro e coordinandosi con altri elementi della stessa natura;
tra tutti i \texttt{Manager} è presente un nodo \texttt{Leader}, che ha il compito di coordinare tra loro i vari \texttt{Manager}.
Se invece si vuole che il nodo si occupi solo di eseguire i servizi richiesti, allora conviene assegnargli il ruolo di \texttt{Worker}, così facendo le risorse del nodo verranno interamente impiegate per la realizzazione di uno o più servizi.

Passando poi al funzionamento dell’insieme dei nodi nel cluster, occorre specificare che la gestione del sistema (o \textit{Orchestration} che dir si voglia) avviene tramite specifiche \texttt{API} per etichettare i ruoli dei singoli nodi, uno \texttt{scheduler} che si occupa di creare i vari task e di gestirli a seconda dello stato del sistema e infine un \texttt{dispatcher}.
Il compito di quest'ultimo è mandare in esecuzione sui nodi i task, o microservizi, secondo le istruzioni dello scheduler e sorvegliare l’andamento del sistema tramite una serie di controlli periodici;
qualora uno dei controlli non vada a buon fine e un container e/o il nodo su cui è in esecuzione risulti compromesso, il sistema provvede automaticamente a rischedulare il lavoro del componente compromesso su quelli rimasti attivi.
Per quanto riguarda infine la resilienza agli errori, ogni \textit{container} (o \texttt{task} che dir si voglia) può essere replicato su più nodi all'interno dello \textit{Swarm}; tali repliche sono in tutto e per tutto equivalenti e sono impiegate sia nelle procedure di gestione e ripristino dell'errore di uno o più \textit{container} sia nella distribuzione del carico di lavoro sui nodi effettuata dall'\textit{Orchestrator}.
Tale componente si occupa a tempo di esecuzione di smistare le richieste sui nodi del cluster in base al traffico e allo stato di ogni nodo del sistema.

La \Cref{fig:dockerSwarmExample} mostra un esempio di \textit{Swarm} composto da diversi nodi su cui sono in esecuzione diversi \textit{container}, anche detti \texttt{task} o microservizi:

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.45]{./res/replicated-vs-global.png}
  \caption{Esempio di Cluster Swarm con microservizi e nodi di natura differente (fonte \href{https://docs.docker.com/engine/swarm/how-swarm-mode-works/services/\#replicated-and-global-services}{Link})}%
  \label{fig:dockerSwarmExample}
\end{figure}

\subsubsection{Distribuzione dei database SQL}\label{subsub:sql-distribution}

Per quanto riguarda la distribuzione dei database, abbiamo scelto di affidarci alle tecniche di replicazione messe a disposizione dai diversi database SQL\@.
Anche qui, il problema trattato è parecchio complesso e non esiste una soluzione univoca, ma ogni database ha il proprio meccanismo di replicazione e sharding dei dati.

\textbf{Microsoft SQL} è sicuramente uno dei database relazionali più valido in circolazione e anche nell'approccio alla distribuzione mette in campo un'architettura efficace~\cite{sql-replication-model}:
ispirato al design pattern \textit{publish-subscribe}, il modello in questione prevede la presenza di uno o più server \textit{publisher}
che distribuiscono tramite un \textit{distributor server} parti di uno stesso database chiamate \textit{publications};
queste ultime non sono altro che raggruppamenti di \textit{articles}, a loro volta collezioni di tabelle, \texttt{stored procedure} e viste;
destinatari finali delle \textit{publications} sono i server \textit{subscribers} che mantengono una copia locale dei dati in questione.
Questa architettura viene poi arricchita dalle tipologie di replicazione~\cite{replication-type},
che permettono di apportare modifiche di natura sostanziale all'architettura sopra descritta,
ad esempio abilitando i \textit{subscribers} a poter essere utilizzati come elementi attivi nelle scritture e non solo componenti di sola lettura.

Alternativa \textit{open-source} alla soluzione sopracitata è il database \textbf{MySQL}: anche quest'ultimo mette a disposizione diversi modelli di replicazione~\cite{MySQL-replication}, come \textit{master-slave}, \textit{master-master} e \textit{Group Replication};
nel caso in cui si scelga \textbf{MariaDB}, fork di MySQL, allora è a disposizione anche \textit{Galera Cluster}.
Ciascuno di questi modelli si adatta meglio a specifici contesti di distribuzione mentre arranca in altri;
in ogni caso tutte le possibilità sopracitate si rifanno a modelli ben noti in letteratura,
di conseguenza è più facile la scelta a seconda dello scenario di utilizzo rispetto che su Microsoft SQL e la sua architettura proprietaria.

A prescindere dallo specifico RDBMS utilizzato, è importante evidenziare che la replicazione è trasparente ai client:
per quanto possano cambiare le tecniche di replicazione e gestione dei differenti nodi del DB, ciascun client può collegarsi a uno degli host e accedere ai dati come se si trattasse di un'istanza singola.
In molti casi è comunque consigliato utilizzare come punto di accesso al cluster di database un \textit{load balancer} che permetta di distribuire gli accessi nel modo più efficiente possibile.

\begin{figure}[H]
  \centering
  \includegraphics[scale=0.5]{./res/mysql-master-slave.png}
  \caption{MariaDB Galera Cluster replication}%
  \label{fig:mysql-master-slave}
\end{figure}

Come è possibile vedere in \Cref{fig:mysql-master-slave}, nel quale è rappresentato il meccanismo di replicazione fornito da Galera Cluster di MariaDB, la replicazione è bidirezionale tra i vari master, di conseguenza ogni nodo consente l'accesso sia in lettura che in scrittura;
il processo di replicazione, gestito dalle \textit{write set replication API} (\texttt{wsrep}), è completamente trasperente al client, il quale può accedere dove preferisce.

\subsection{Comportamento}\label{subsec:behavior}
L'architettura da noi realizzata non ha richiesto nessun cambiamento
nel comportamento dei moduli in questione nel sistema LPaaS.
Tutti i problemi legati alla natura distribuita del sistema, quali replicazione, distribuzione del carico di lavoro,
resilienza e infine consistenza sono a carico delle piattaforme dentro cui i moduli sono inseriti, rimanendo trasparenti a questi ultimi.


\subsection{Interazione}\label{subsec:interaction}
Anche qui vale lo stesso discorso fatto per il comportamento del sistema: ai moduli rimane trasparente ogni modifica necessaria a rendere il sistema distributo.
Unica eccezione a ciò la necessità di configurare il modulo di Buisness Logic per l'interazione con un database remoto invece che locale, problema che però risulta più di natura implementativa e di conseguenza sarà trattato nella sezione adeguata.


\subsubsection{Distribuzione}\label{subsub:interaction:distribuition}
La pianificazione modulare descritta nella \Cref{subsec:structure} e le tecnologie descritte
nelle \Cref{subsub:swarmmode,subsub:sql-distribution} sono integrabili in maniera assolutamente
trasparente a LPaaS.

Grazie alla divisione in componenti, i due moduli, Buisness Logic e Database,
possono essere distribuiti tramite due piattaforme differenti che tra di loro non hanno nulla in comune.

Queste due piattaforme rimangono però tra loro assolutamente compatibili:
all'applicativo in esecuzione su \textit{Docker Swarm mode} è indifferente se il database sia un singolo host, un cluster replicato o un'immagine Docker (non replicata) nel medesimo Swarm;
analogamente, al modulo database è ignoto quale replica dell'applicativo LPaaS sta scrivendo o leggendo i dati.

Il sistema quindi può essere definito come \textit{loosely coupled}, rendendolo difatti in linea con i
principi di progettazione di un'architettura a microservizi.

\subsection{Soluzioni scartate}\label{subsub:discardedsol}
Prima di giungere a una soluzione funzionante, abbiamo effettuato diverse ricerche e sperimentato varie possibilità:
in questa sottosezione tratteremo di quelle che sono state le due maggiori strade inizialmente percorse per la progettazione del nostro sistema e quali sono state le ragioni per cui abbiamo deciso di scartale.
Va premesso che l'integrazione tra Docker e un approccio \textit{stateful} è un tema decisamente dibattuto all'interno della community di Docker, di conseguenza non è stato immediato trovare una linea chiara e definita su cosa fosse e non fosse possibile realizzare e soprattutto sul come farlo.

\subsubsection{Infinit}\label{subsub:infinit}
I \textit{container} sono trattati come \textit{stateless} da Docker,
tuttavia dalle ultime versioni della piattaforma in questione è presente un supporto,
se pure non ancora a tutto tondo, della gestione dello stato interno di un \textit{container};
tale tecnologia prende il nome di \textit{Docker Volumes}~\cite{docker-volumes}.

Il principio di funzionamento è molto semplice: utilizzando il filesystem \textit{UnionFS} già citato
si sovrappone una directory esterna a un \textit{container} ad una al suo interno;
ogni aggiunta/rimozione/modifica dei files all'interno di questa directory condivisa viene fatta sia all'interno del \textit{container} che della directory esterna.

L'operazione di aggiunta, o \textit{mount}, di uno o più volumi è possibile anche in modalità di esecuzione \textit{swarm},
specificando gli opportuni paramentri di configurazione al lancio del servizio, in via teorica sarebbe stato possibile quindi mantenere lo stato
senza estrarlo dal sistema e delegarlo a un provider di database esterno, rendendo dunque il tutto più compatto e interamente gestito dalla piattaforma Docker.

Tuttavia questa soluzione comporta un problema tutt'altro che banale:
nel momento in cui più repliche di una stessa immagine sono in esecuzione all'interno di uno \textit{swarm}, l'\textit{Orchestrator} del cluster decide
a tempo di esecuzione a quale replica affidare i task ricevuti sulla base di molteplici fattori.
Sta di fatto però che lo stato interno delle repliche non viene in alcun modo condiviso, di conseguenza lo stato interno del sistema non verrà mai reso consistente e per un utente che accede al sistema lo stato in questione
sarà quello della replica che servirà la sua richiesta; potrebbe addirittura capitare che uno stesso utente non sia in grado di vedere le modifiche precedentemente apportate da lui al sistema
per colpa di un cambio nell'instradamento delle sue richieste ad una differente replica rispetto alla precedente.

La prima soluzione che abbiamo pensato è stata quella di realizzare manualmente la sincronizzazione dello stato
mappando tutte le directory contenenti i dati all'interno dei differenti \textit{container} sulla stessa directory esterna;
inutile dire però che questa tecnica presentava due grosse problematiche.
In primo luogo centralizzando le operazioni di lettura e scrittura in un unica directory si sarebbero venuti a creare enormi colli di bottiglia nell'accesso ai dati e conseguenti cali di performances;
successivamente il sistema così configurato sarebbe risultato assolutamente fragile dal punto di vista delle corse critiche nei processi di accesso ai dati, cosa che avrebbe reso lo stato in una continua inconsistenza.

Scartata questa proposta, abbiamo cercato quali potessero essere eventuali soluzioni al problema proposte da utenti della community Docker sicuramente più esperti di noi.
Leggendo tra i vari forum, ci siamo imbattuti più volte nel nome di Infinit~\cite{Infinit}, piattaforma di storage software distribuito che permette di creare strutture di memorizzazione flessibili e scalabili,
tra cui \textit{volumes} integrabili all'interno dei \textit{container} Docker\footnote{\url{https://blog.docker.com/2017/01/docker-storage-infinit-faq/}}.

Infinit sembrava offrire esattamente la soluzione al problema sopra descritto,
tuttavia nel momento in cui abbiamo inziato alla configurazione del \textit{volume} remoto che avrebbe ospitato lo stato, ci siamo imbattuti in diversi problemi a cui non trovavamo soluzione.
Abbiamo quindi aperto una \textit{issue}\footnote{\url{https://github.com/infinit/infinit/issues/55}} sul \href{https://github.com/infinit/infinit}{repository ufficiale Git di Infinit} sperando di risolvere il nostro problema.
Il risultato tuttavia è stato differente: il nostro post ha ricevuto diverse risposte di utenti con il nostro stesso problema e la richiesta di una soluzione;
analizzando poi la \textit{issue} precedente alla nostra\footnote{\url{https://github.com/infinit/infinit/issues/54}} abbiamo dedotto che il progetto Infinit
fosse da considerarsi come non più supportato da diversi mesi, rendendo difatti il nostro problema irrisolvibile.

Alla luce di ciò, abbiamo abbandonato l'idea di volumi condivisi e abbiamo verificato altre ipotesi.



\subsubsection{Kubernetes}\label{subsub:kubernetes}

Piattaforma per distribuzione e scaling di \textit{containers} alternativa a \textit{Docker swarm},
Kubernetes~\cite{Kubernetes} poteva essere la tecnologia adatta alla risoluzione del nostro problema.
Analizzandone l'architettura e le modalità di funzionamento, abbiamo notato come Kubernetes costituisca uno dei prodotti più completi per
la gestione di applicazioni su \textit{containers} distribuiti:
in particolare per quanto riguarda l'approccio a servizi \textit{stateful},
Kubernetes matura il concetto di \textit{volumes} presente su Docker adattandolo al contesto distribuito; nella documentazione
si parla infatti di \textit{StatefulSets}\footnote{\url{https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/?spm=a2c65.11461447.0.0.18076370KGUqNH}}, oggetto che espone API per la gestione di uno stato distribuito.

Nonostante questa soluzione ci sembrasse promettente, abbiamo scelto di non adottarla per via delle poche garanzie che abbiamo trovato sul suo effettivo funzionamento:
dopo l'esperienza con Infinit descritta nella sezione sopra non ci sembrava opportuno adottare ulteriori soluzioni
di natura sperimentale senza avere prima certezze sul loro funzionamento che andassero oltre le Live Demo tenute alle conferenze dove vengono
presentate le tecnologie in questione.

C'è tuttavia anche un'ulteriore ragione all'accantonamento di questa soluzione, di natura più teorico/concettuale:
dal momento in cui abbiamo iniziato a lavorare al progetto alla stestura di questa relazione è trascorso circa un anno e mezzo, tempo
in cui abbiamo avuto modo di approfondire le nostre conoscenze nell'ambito della progettazione di architetture distribuite e delle basi teoriche che vi stanno dietro.
Alla luce di ciò e della nostra analaisi su LPaaS, abbiamo ritenuto opportuno non concentrare tutto il sistema in un solo modello come avevamo pensato all'inizio,
ma al contrario riconoscere che all'interno del progetto esistevano moduli diversi che dovevano essere trattati con modalità e tecniche differenti tra loro.
